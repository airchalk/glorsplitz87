import os
import logging

from sqlalchemy import Column, ForeignKey, Integer, String, DateTime, Boolean, MetaData, Float
from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.pool import Pool, NullPool
from sqlalchemy.orm import relationship
from sqlalchemy import create_engine, exists
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)

Base = declarative_base()

DATABASE_CONNECTION_STRING = os.getenv('DATABASE_CONNECTION_STRING','')

Base = declarative_base(metadata=MetaData(schema='facedub'))

# global connection to the database, so can serve as a connection pool between requests
# individual lambda functions create their own sessions, though
# also a separate asyncio connection to database
try:
	engine = create_engine(DATABASE_CONNECTION_STRING, echo=True, poolclass=NullPool)
	DBSession = sessionmaker(bind=engine)
except Exception as e:
	logger.error(e, exc_info=True)

# reflection takes too long - 5 seconds
"""
Base = automap_base(metadata=MetaData(schema='facedub'))
engine = create_engine(DATABASE_CONNECTION_STRING, echo=True, poolclass=NullPool)
# reflect the tables
Base.prepare(engine, reflect=True)
# mapped classes are now created with names by default
# matching that of the table name.
Videos = Base.classes.videos
Users = Base.classes.users
Swaps = Base.classes.swaps
Settings = Base.classes.settings
Images = Base.classes.images
Faces = Base.classes.faces
Dubs = Base.classes.dubs
"""

class Users(Base):
	__tablename__ = 'users'
	id = Column(String, primary_key=True)
	display_name = Column(String)
	email = Column(String)
	phone_number = Column(String)
	first_name = Column(String)
	last_name = Column(String)
	signup_date = Column(DateTime(timezone=True))
	picture_url = Column(String)
	billing_record = Column(JSONB)
	email_verified = Column(Boolean)
	billing_id = Column(String)
	billing_account_balance = Column(Float)

class Faces(Base):
	__tablename__ = 'faces'
	id = Column(Integer, primary_key=True, autoincrement=True)
	image_id = Column(Integer, ForeignKey('image.id'))
	video_id = Column(Integer, ForeignKey('video.id'))
	name = Column(String)
	video_first_appearance_timestamp_seconds = Column(Float)
	bounding_box_top_left_x = Column(Integer)
	bounding_box_top_left_y = Column(Integer)
	bounding_box_top_right_x = Column(Integer)
	bounding_box_top_right_y = Column(Integer)
	bounding_box_bottom_left_x = Column(Integer)
	bounding_box_bottom_left_y = Column(Integer)
	bounding_box_bottom_right_x = Column(Integer)
	bounding_box_bottom_right_y = Column(Integer)
	model_estimated_age = Column(Integer)
	model_estimated_gender = Column(String)
	model_estimated_expression = Column(String)
	model_name = Column(String)
	model_version = Column(String)
	landmarks = Column(JSONB)
	model_run_date = Column(DateTime(timezone=True))

class Videos(Base):
	__tablename__ = 'videos'
	id = Column(Integer, primary_key=True, autoincrement=True)
	user_id = Column(String, ForeignKey('users.id'))
	upload_date = Column(DateTime(timezone=True))
	file_location_url = Column(String)
	file_size_bytes = Column(Integer)
	file_type = Column(String)
	file_name = Column(String)
	file_encoding_codec = Column(String)
	duration_seconds = Column(Integer)
	uuid = Column(String)
	resolution_horizontal = Column(Integer)
	resolution_vertical = Column(Integer)

class Swaps(Base):
	__tablename__ = 'swaps'
	id = Column(Integer, primary_key=True, autoincrement=True)
	user_id = Column(String, ForeignKey('users.id'))
	request_id = Column(String)
	request_date = Column(DateTime(timezone=True))
	source_video_id = Column(Integer, ForeignKey('videos.id'))
	source_image_id = Column(Integer)
	replace_background_flag = Column(Boolean)
	background_image_id = Column(Integer, ForeignKey('images.id'))
	background_video_id = Column(Integer, ForeignKey('videos.id'))
	result_video_id = Column(Integer, ForeignKey('videos.id'))
	name = Column(String)
	source_video_face_id = Column(Integer, ForeignKey('faces.id'))
	source_image_face_id = Column(Integer, ForeignKey('faces.id'))
	status = Column(String)
	video_template = Column(String)
	face_template = Column(String)

class Settings(Base):
	__tablename__ = 'settings'
	id = Column(Integer, primary_key=True, autoincrement=True)
	user_id = Column(String, ForeignKey('users.id'))
	key = Column(String)
	value = Column(JSONB)

class Images(Base):
	__tablename__ = 'images'
	id = Column(Integer, primary_key=True, autoincrement=True)
	user_id = Column(String, ForeignKey('users.id'))
	upload_date = Column(DateTime(timezone=True))
	file_location_url = Column(String)
	file_type = Column(String)
	file_size_bytes = Column(Integer)
	resolution_horizontal = Column(Integer)
	resolution_vertical = Column(Integer)

class Dubs(Base):
	__tablename__ = 'dubs'
	id = Column(Integer, primary_key=True, autoincrement=True)
	user_id = Column(String, ForeignKey('users.id'))
	name = Column(String)
	request_date = Column(DateTime(timezone=True))
	source_video_id = Column(Integer, ForeignKey('videos.id'))
	replace_background_flag = Column(Boolean)
	background_image_id = Column(Integer)
	background_video_id = Column(Integer, ForeignKey('videos.id'))
	result_video_id = Column(Integer, ForeignKey('videos.id'))
	source_language = Column(String)
	request_languages = Column(JSONB)
	request_text_to_speech = Column(JSONB)
	source_video_face_id = Column(Integer)

class Subscriptions(Base):
	__tablename__ = 'subscriptions'
	id = Column(Integer, primary_key=True, autoincrement=True)
	user_id = Column(String, ForeignKey('users.id'))
	plan = Column(String)
	stripe_subscription_id = Column(String)
	start_date = Column(DateTime(timezone=True))
	end_date = Column(DateTime(timezone=True))
	discount_code = Column(String)

class Payments(Base):
	__tablename__ = 'payments'
	id = Column(Integer, primary_key=True, autoincrement=True)
	user_id = Column(String, ForeignKey('users.id'))
	transaction_type = Column(String)
	transaction_date = Column(DateTime(timezone=True))
	transaction_amount_usd = Column(Float)
	subscription_id = Column(Integer, ForeignKey('subscriptions.id'))
	transaction_success_flag = Column(Boolean)

def connect_to_database():
	"""
	new session for each request

	we could use sqlalchemy event hooks
	for pre-commit/post-commit, but this is simpler
	as long as we always use this function
	"""
	try:
		session = DBSession()
		return session
	except Exception as e:
		logger.error(e, exc_info=True)

def get_or_create(session, model, **kwargs):
	instance = session.query(model).filter_by(**kwargs).first()
	if instance:
		return instance
	else:
		instance = model(**kwargs)
		session.add(instance)
		session.commit()
		return instance

def get_or_create_with_flag(session, model, **kwargs):
	instance = session.query(model).filter_by(**kwargs).first()
	if instance:
		return instance, False
	else:
		instance = model(**kwargs)
		session.add(instance)
		session.commit()
		return instance, True
