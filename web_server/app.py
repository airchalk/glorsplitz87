import subprocess
#from starlette.applications import Starlette
#from starlette.responses import JSONResponse
#from starlette.endpoints import HTTPEndpoint
#import uvicorn
from flask import Flask, request, jsonify, g, abort
import os, sys
import os.path
import shutil
import boto3
import logging
import uuid
import glob
import json
import requests
from datetime import datetime
from models import *
from flask_cors import CORS
import stripe
from functools import wraps

#When running on GCP Cloud Run, you write logs from your service, they will be picked up automatically by Stackdriver Logging so long as the logs are written to any of these locations:
#Standard output (stdout) or standard error (stderr) streams
#but to get more details like line no, etc. we use cloud logging
logging.getLogger('s3transfer').setLevel(logging.CRITICAL)

from logger import setup_logging
log = setup_logging()

import azure.storage.blob as azureblob
import azure.batch._batch_service_client as batch
import azure.batch.batch_auth as batch_auth
import azure.batch.models as batchmodels
import firebase_admin
from firebase_admin import credentials
from firebase_admin import auth

PORT = os.environ.get('PORT', 8000)
JOB_ID = 'CPU_nonprefetched_free' # CPU_paid
INPUT_S3_BUCKET = os.getenv('INPUT_S3_BUCKET')
OUTPUT_S3_BUCKET = os.getenv('OUTPUT_S3_BUCKET')
AWS_ACCESS_KEY_ID = os.getenv('AWS_ACCESS_KEY_ID')
AWS_SECRET_ACCESS_KEY = os.getenv('AWS_SECRET_ACCESS_KEY')
BATCH_ACCOUNT_NAME = os.getenv('BATCH_ACCOUNT_NAME')
BATCH_ACCOUNT_KEY = os.getenv('BATCH_ACCOUNT_KEY')
BATCH_ACCOUNT_URL = os.getenv('BATCH_ACCOUNT_URL')
DOCKER_IMAGE_URL = os.getenv('DOCKER_IMAGE_URL')
DOCKER_TAG = os.getenv('DOCKER_TAG')
DATABASE_EVENT_WEBHOOK_AUTH = os.getenv('DATABASE_EVENT_WEBHOOK_AUTH')
ONESIGNAL_APP_ID = os.getenv('ONESIGNAL_APP_ID')
ONESIGNAL_API_KEY = os.getenv('ONESIGNAL_API_KEY')
MAILGUN_API_KEY = os.getenv('MAILGUN_API_KEY')
MAILGUN_API_URL = os.getenv('MAILGUN_API_URL')
MAILGUN_FROM_EMAIL = os.getenv('MAILGUN_FROM_EMAIL')
APP_NAME = os.getenv('APP_NAME')

stripe.api_key = os.getenv('STRIPE_KEY','')

# Get the service client
s3 = boto3.client('s3', aws_access_key_id=AWS_ACCESS_KEY_ID, aws_secret_access_key=AWS_SECRET_ACCESS_KEY, region_name='us-west-2')

batch_credentials = batch_auth.SharedKeyCredentials(BATCH_ACCOUNT_NAME, BATCH_ACCOUNT_KEY)
batch_client = batch.BatchServiceClient(batch_credentials, batch_url=BATCH_ACCOUNT_URL)
#app = Starlette(debug=True)
#Tensor Tensor("Placeholder_12:0", shape=(3, 3, 3, 32), dtype=float32) is not an element of this graph.
#https://stackoverflow.com/questions/51127344/tensor-is-not-an-element-of-this-graph-deploying-keras-model
#https://stackoverflow.com/questions/47115946/tensor-is-not-an-element-of-this-graph
#https://github.com/tensorflow/tensorflow/issues/14356
cred = credentials.Certificate(os.getenv('FIREBASE_APPLICATION_CREDENTIALS'))
firebase_app = firebase_admin.initialize_app(cred)

app = Flask(__name__)
CORS(app)

def authorize(f):
	@wraps(f)
	def decorated_function(*args, **kws):
		if not 'Authorization' in request.headers:
			abort(401)

		token = request.headers.get('Authorization')
		user_id = get_user_id(token)
		if not user_id:
			abort(401)
		else:
			g.user_id = user_id

		return f(*args, **kws)
	return decorated_function

@app.route("/face_templates", methods=['GET'])
def get_face_templates():
	"""
	"""
	try:
		# query S3 for all files in face_templates folder
		# return first image of each person
		faces = []
		s3_resource = boto3.resource('s3')
		bucket = s3_resource.Bucket(INPUT_S3_BUCKET)
		prefix = 'face_templates/'
		url_prefix = 'https://s3-us-west-2.amazonaws.com/uploads.facedub.app/'
		for file in bucket.objects.filter(Prefix = prefix):
			log.debug('face templates key:' + str(file.key))
			if prefix != file.key and prefix != file.key.strip('/'): # not the folder itself, or folder with backslash
				if file.key.find('_1.')!=-1:
					parts = file.key.split('/')[1].split('_')
					faces.append({'name': parts[0] + ' ' + parts[1], 'url': url_prefix + file.key})
		return jsonify(faces)
	except Exception as e:
		log.error(e, exc_info=True)

@app.route("/process_payment", methods=['POST'])
@authorize
def process_payment():
	"""
	stripe_token: stripe_token,
	charge_amount: self.charge_amount,
	receipt_email: self.user.email
	"""
	try:
		user_id = g.get('user_id')

		body = request.json
		stripe_token = body.get('stripe_token')
		charge_amount = body.get('charge_amount')
		receipt_email = body.get('receipt_email')
		full_name = body.get('full_name')

		session = connect_to_database()
		customer = get_or_create(session, Users, id=user_id)
		billing_customer_id = customer.billing_id
		if not billing_customer_id:
			billing_customer_id = create_customer_billing_record(user_id, stripe_token, full_name, receipt_email)
			customer.billing_id = billing_customer_id
			session.commit()
		else:
			update_customer_credit_card(billing_customer_id, stripe_token)
		
		result = charge_credit_card(user_id, charge_amount, stripe_token, billing_customer_id, receipt_email)
		payment_record = get_or_create(session, Payments, user_id=user_id, transaction_date=datetime.utcnow(), transaction_type='credit', transaction_amount_usd=charge_amount, transaction_successful=result)
		session.close()
		return jsonify(result)
	except Exception as e:
		log.error(e, exc_info=True)

def create_customer_billing_record(user_id, stripe_token, customer_name, receipt_email):
	"""
	https://stripe.com/docs/api#create_customer
	"""
	try:
		log.debug('creating stripe customer with token: ' + str(stripe_token))

		if stripe_token:
			customer = stripe.Customer.create(
			  source=stripe_token.get('id'),
			  description=customer_name,
			  email=receipt_email,
			  metadata={'user_id': user_id}
			)
		elif receipt_email:
			customer = stripe.Customer.create(
			  description=customer_name,
			  email=receipt_email,
			  metadata={'user_id': user_id}
			)
		else:
			customer = stripe.Customer.create(
			  description=customer_name,
			  metadata={'user_id': user_id}
			)
		return customer.id
	except Exception as e:
		log.error(e, exc_info=True)

def update_customer_credit_card(stripe_customer_id, stripe_token):
	try:
		log.debug('in update_customer_credit_card')
		customer_record = stripe.Customer.retrieve(stripe_customer_id)
		if customer_record and stripe_token:
			# update the token for the customer, in case the card has changed
			customer_record.source = stripe_token.get('id')
			customer_record.save()
			return True
		else:
			log.debug('couldnt update customer credit card - missing data')
			return False
	except Exception as e:
		log.error(e, exc_info=True)

def charge_credit_card(user_id, charge_amount, token, customer_id, receipt_email):
	"""
	https://stripe.com/docs/api/charges/create
	"""
	try:
		result = stripe.Charge.create(
		amount=charge_amount*100,#uses cents
		currency="usd",
		source=token, # obtained with Stripe.js
		customer=customer_id,
		receipt_email=receipt_email,
		description="Charge for FaceDub.app"
		)
		log.debug(result)
		if result.status=='succeeded':
			# update the account balance
			record = get_or_create(session, Users, id=user_id)
			balance = record.account_balance
			if balance is None:
				balance = 0
			balance += charge_amount
			record.account_balance = balance
			session.add(record)
			session.commit()
			session.close()
			return True
		else:
			return False
	except Exception as e:
		log.error(e, exc_info=True)
		return False

@app.route("/account_balance", methods=['GET'])
@authorize
def account_balance():
	"""
	"""
	try:
		user_id = g.get('user_id')
		account_balance = 0
		if user_id:
			customer = get_or_create(session, Users, id=user_id)
			account_balance = customer.billing_account_balance
			if account_balance is None:
				account_balance = 0
		return jsonify(account_balance)
	except Exception as e:
		log.error(e, exc_info=True)

@app.route("/debit_account_balance", methods=['POST'])
@authorize
def debit_account_balance():
	"""
	"""
	try:
		user_id = g.get('user_id')

		result = False
		body = request.json
		debit_amount = body.get('debit_amount')
		account_balance = 0
		if user_id:
			customer = get_or_create(session, Users, id=user_id)
			account_balance = customer.billing_account_balance
			if account_balance is None:
				account_balance = 0
			if account_balance > 0 and (account_balance - debit_amount) >= 0:
				account_balance = account_balance - debit_amount
				customer.billing_account_balance = account_balance
				result = True

				# update the payments table too
				payment_record = get_or_create(session, Payments, user_id=user_id, transaction_date=datetime.utcnow(), transaction_type='debit', transaction_amount_usd=debit_amount, transaction_successful=result)
		return jsonify(result)
	except Exception as e:
		log.error(e, exc_info=True)

@app.route("/send_task", methods=['POST'])
@authorize
def send_task():
	"""
	"""
	try:
		user_id = g.get('user_id')
		
		body = request.json
		request_id = body.get('request_id')
		video_template = body.get('video_template')
		face_template = body.get('face_template')

		TASK_ID = str(uuid.uuid4())

		if face_template:
			name = face_template.get('name')
			face_template = '_'.join(name.split(' '))
		session = connect_to_database()
		#responses = session.query(Notifications).filter(Notifications.store_id==store_id).all()
		record = Swaps(user_id = user_id, request_id = request_id, request_date = datetime.utcnow(), status='submitted', video_template=video_template, face_template=face_template)
		session.add(record)
		session.commit()

		log.debug('sending task with image of docker tag:' + str(DOCKER_TAG))

		# This is the user who run the command inside the container.
		user = batchmodels.AutoUserSpecification(scope=batchmodels.AutoUserScope.task, elevation_level=batchmodels.ElevationLevel.non_admin)

		# This is the docker image we want to run
		task_container_settings = batchmodels.TaskContainerSettings(image_name='hello-world' + ':' + 'latest', container_run_options='--rm')

		# The container needs this argument to be executed
		task = batchmodels.TaskAddParameter(id=TASK_ID, command_line='conda run -n env python /run/worker.py ' + str(request_id), container_settings=task_container_settings, user_identity=batchmodels.UserIdentity(auto_user=user))

		batch_client.task.add(JOB_ID, task)
		return jsonify({'result': True})
	except Exception as e:
		log.error(e, exc_info=True)

@app.route("/events/swaps_update", methods=['POST'])
def swaps_updated():
	"""
	{'id': 'ea232f8b-88b9-40eb-a434-1f4e199b6eca', 'table': {'schema': 'facedub', 'name': 'swaps'}, 'trigger': {'name': 'swaps_update'}, 'event': {'session_variables': {'x-hasura-role': 'admin'}, 'op': 'UPDATE', 'data': {'old': {'background_image_id': None, 'status': None, 'source_image_id': None, 'request_date': '2019-09-22T22:10:38.742037+00:00', 'background_video_id': None, 'request_id': None, 'source_video_id': None, 'name': None, 'source_video_face_id': None, 'id': 7, 'result_video_id': None, 'user_id': 'Va1HkziVaeepA0v9SnbPZiuQblX2', 'replace_background_flag': None, 'source_image_face_id': None}, 'new': {'background_image_id': None, 'status': None, 'source_image_id': None, 'request_date': '2019-09-22T22:10:38.742037+00:00', 'background_video_id': None, 'request_id': None, 'source_video_id': None, 'name': 'testnh', 'source_video_face_id': None, 'id': 7, 'result_video_id': None, 'user_id': 'Va1HkziVaeepA0v9SnbPZiuQblX2', 'replace_background_flag': None, 'source_image_face_id': None}}}, 'delivery_info': {'current_retry': 0, 'max_retries': 0}, 'created_at': '2019-10-01T05:52:31.692681Z'}
	"""
	try:
		token = request.headers.get('Authorization')
		if token==DATABASE_EVENT_WEBHOOK_AUTH:
			body = request.json
			log.debug('swaps_update')
			log.debug(body)
			# if status=='processed', grab the 'request_id' and 'user_id' and 'result_video_id'
			old_data = body.get('event',{}).get('data',{}).get('old',{})
			new_data = body.get('event',{}).get('data',{}).get('new',{})
			if old_data.get('status')!='processed' and new_data.get('status')=='processed':
				log.debug('new status is processed, sending notification to user')
				# email the user
				session = connect_to_database()
				user_record = get_or_create(session, Users, id=new_data.get('user_id'))
				email_address = user_record.email

				# video_uuid
				video_record = get_or_create(session, Videos, id=new_data.get('result_video_id'))
				video_uuid = video_record.uuid

				if email_address and video_uuid:
					send_video_completed_notification('email', email_address, video_uuid)
				else:
					log.debug('email or video_uuid are null - not sending notification')
			else:
				pass
		else:
			log.error('Unauthorized call to events/swaps_update')
		return jsonify({'result': True})
	except Exception as e:
		log.error(e, exc_info=True)

@app.route("/upload_to_s3", methods=['POST'])
def upload_to_s3():
	"""
	generate pre-signed URL
	"""
	try:
		data = request.json
		log.debug(data)
		content_type = data.get('contentType')
		filename = data.get('filename')
		upload_type = data.get('upload_type') # 'image' or 'video'
		request_uuid = data.get('request_uuid')

		extension = ''
		if content_type in ['image/png','image/gif','image/jpeg','image/bmp','image/webp','video/mp4']:
			extension = '.' + content_type.split('/')[-1]
		elif filename:
			extension = ''
			"""parts = filename.split('.')
			if len(parts) > 1 and len(parts[-1]) >=3 and len(parts[-1] <=4):
				extension = '.' + parts[-1]"""

		if upload_type == 'image':
			key = request_uuid + '/face_images/' + str(uuid.uuid4()) + extension
		elif upload_type == 'video':
			key = request_uuid + '/original_video' + extension

		# Get the service client
		s3 = boto3.client('s3', aws_access_key_id=AWS_ACCESS_KEY_ID, aws_secret_access_key=AWS_SECRET_ACCESS_KEY)

		url = s3.generate_presigned_url(
			ClientMethod='put_object',
			Params={
				'Bucket':INPUT_S3_BUCKET,
				'Key': key,
				'ACL': 'public-read',
				'ContentType': content_type
				}
		)

		return url
	except Exception as e:
		log.error(e, exc_info=True)

@app.route("/create_customer", methods=['POST'])
def create_customer():
	"""
	 { user_id: user.uid,
	   name: user.displayName,
	   picture: user.photoURL,
	   email: user.email,
	   email_verified: user.emailVerified,
	"""
	try:
		data = request.json
		log.debug(data)
		display_name = data.get('display_name')
		email = data.get('email')
		user_id = data.get('user_id')
		picture = data.get('picture')
		email_verified = data.get('email_verified', False)
		admin_key = data.get('admin_key')
		if admin_key == 'GlorSplitz57':
			log.debug('connecting to db')
			session = connect_to_database()
			#responses = session.query(Notifications).filter(Notifications.store_id==store_id).all()
			record = Users(id = user_id, display_name = display_name, email = email, email_verified = email_verified, picture_url = picture, signup_date = datetime.utcnow())
			session.add(record)
			session.commit()
	except Exception as e:
		log.error(e, exc_info=True)
	finally:
		if session:
			session.close()

def send_video_completed_notification(channel_type, email_address, video_uuid):
	"""
	"""
	try:
		video_url = 'https://app.facedub.app/video/' + str(video_uuid)
		if channel_type=='email':
			# send directly via via Mailgun
			data = {"from": MAILGUN_FROM_EMAIL,
			"to": [email_address],
			"subject": "Your video is ready!",
			"html": "Please <a href='" + video_url + "'>click here</a> to view your video.<br/><br/>the " + APP_NAME + " team"}
			r = requests.post(MAILGUN_API_URL, auth=("api", MAILGUN_API_KEY), data=data)
			if r.status_code == 200:
				log.debug(r.json())
			else:
				log.error(r.text)
		elif channel_type=='web_push':
			url = 'https://onesignal.com/api/v1/notifications'
			headers = {'Authorization': 'Basic ' + ONESIGNAL_API_KEY}
			data = {
				'app_id': ONESIGNAL_APP_ID,
				'contents': 'Please click here to view your video',
				'headings': 'Your video is ready!',
				#'big_picture': ,
				#'chrome_web_image': ,
				'url': video_url,
				"filters": [
			  		{"field": "email", "key": "email", "relation": "=", "value": email_address}, 
				]}
			r = requests.post(url, headers=headers, data=json.dumps(data))
			if r.status_code == 200:
				result = r.json()
			else:
				log.error(result)
	except Exception as e:
		log.error(e, exc_info=True)

def get_user_id(id_token):
	try:
		decoded_token = auth.verify_id_token(id_token)
		uid = decoded_token['uid']
		return uid
	except Exception as e:
		log.error(e, exc_info=True)
		return None

@app.route("/", methods=['POST'])
def home():
	"""
	"""
	try:
		return jsonify({'result': True})
	except Exception as e:
		log.error(e, exc_info=True)

if __name__ == '__main__':
	log.debug('starting app.py')
	app.run(host='0.0.0.0', port=PORT)
