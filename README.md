Project:
This project provides code to process videos with deep learning (both face detection & manipulation), and the web server to process requests.


Dependencies:
- ffmpeg
- Python3
- boto3
- flask
- numpy
- tensorflow
- keras
- psycopg2
Services:
- PostgreSQL
- Azure batch
- AWS S3
- Google Cloud logging

How to run:
- Build the Dockerfile inside each folder
- Add your credentials by passing in build arguments to Docker
- Run the Docker containers on GCP Cloud Run or AWS Elastic Beanstalk, etc.
