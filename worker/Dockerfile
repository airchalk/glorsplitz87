#Images built after May 20 2019 (TF nightly, plus TF versions 1.14 and onward) are based on Ubuntu 18.04
#FROM tensorflow/tensorflow:1.14.0-py3-jupyter
#FROM gcr.io/deeplearning-platform-release/tf-cpu.1-14:latest
#FROM 763104351884.dkr.ecr.us-east-1.amazonaws.com/tensorflow-inference:1.14.0-cpu-py36-ubuntu18.04

# for pytorch:
# gcr.io/deeplearning-platform-release/pytorch-gpu.1-0
# gcr.io/deeplearning-platform-release/pytorch-cpu.1-0
# 763104351884.dkr.ecr.us-east-1.amazonaws.com/pytorch-training:1.4.0-gpu-py27-cu101-ubuntu16.04
# https://github.com/aws/sagemaker-pytorch-container/blob/master/docker/1.0.0/base/Dockerfile.gpu
#FROM us.gcr.io/airchalk-204621/tfcpu:latest
#FROM gcr.io/deeplearning-platform-release/pytorch-gpu.1-0:latest
#FROM ubuntu:18.04
FROM nvidia/cuda:10.1-cudnn7-runtime-ubuntu18.04

MAINTAINER Nobody <blah@nobody.com>

ENV TZ=America/San_Francisco
ENV DEBIAN_FRONTEND=noninteractive

# pass in build args
ARG GOOGLE_APPLICATION_CREDENTIALS_RAW
ARG FIREBASE_APPLICATION_CREDENTIALS_RAW
ARG BATCH_ACCOUNT_NAME
ARG BATCH_ACCOUNT_KEY
ARG BATCH_ACCOUNT_URL
ARG INPUT_S3_BUCKET
ARG OUTPUT_S3_BUCKET
ARG AWS_ACCESS_KEY_ID
ARG AWS_SECRET_ACCESS_KEY
ARG DOCKER_IMAGE_URL
ARG DATABASE_CONNECTION_STRING
# set environment variables to build args
ENV GOOGLE_APPLICATION_CREDENTIALS /run/gcp_credentials.json
ENV FIREBASE_APPLICATION_CREDENTIALS /run/firebase_credentials.json
ENV BATCH_ACCOUNT_NAME ${BATCH_ACCOUNT_NAME}
ENV BATCH_ACCOUNT_KEY ${BATCH_ACCOUNT_KEY}
ENV BATCH_ACCOUNT_URL ${BATCH_ACCOUNT_URL}
ENV INPUT_S3_BUCKET ${INPUT_S3_BUCKET}
ENV OUTPUT_S3_BUCKET ${OUTPUT_S3_BUCKET}
ENV AWS_ACCESS_KEY_ID ${AWS_ACCESS_KEY_ID}
ENV AWS_SECRET_ACCESS_KEY ${AWS_SECRET_ACCESS_KEY}
ENV DOCKER_IMAGE_URL ${DOCKER_IMAGE_URL}
ENV DATABASE_CONNECTION_STRING ${DATABASE_CONNECTION_STRING}
ENV CDN_URL_PREFIX ${CDN_URL_PREFIX}

# dlib won't install via pip / takes forever so we prefer to use conda
# Conda packages are binaries. There is never a need to have compilers available to install them
# current default behavior of conda is to allow all global user site-packages to be seen from within environments
# TODO https://jcristharif.com/conda-docker-tips.html

# this image already comes with miniconda installed (python 3.7), so we just update it instead of installing from scratch
# environment name is 'base'
RUN  apt-get update && apt-get install -y wget build-essential
RUN wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
RUN bash Miniconda3-latest-Linux-x86_64.sh -bfp /usr/local
RUN rm Miniconda3-latest-Linux-x86_64.sh

# with the release of Docker 19.03, usage of nvidia-docker2 packages are deprecated since NVIDIA GPUs are now natively supported as devices in the Docker runtime

WORKDIR /run
# all subsequent commands will be from /run

COPY worker/environment_first_order.yml .
RUN conda env create -f environment_first_order.yml
#RUN conda env update --name base --file environment.yml --prune

# Make RUN commands use the new environment:
SHELL ["conda", "run", "-n", "env_first_order", "/bin/bash", "-c"]
RUN pip install --no-dependencies face_recognition
RUN pip install git+https://github.com/ageitgey/face_recognition_models

COPY worker/worker.py .
COPY worker/db_models.py .
COPY worker/google_logger.py .
COPY worker/file_utils.py .
COPY worker/video_utils.py .
COPY worker/inference.py .
RUN echo ${GOOGLE_APPLICATION_CREDENTIALS_RAW} | base64 -d > gcp_credentials.json
RUN echo ${FIREBASE_APPLICATION_CREDENTIALS_RAW} | base64 -d > firebase_credentials.json
#RUN echo '${GOOGLE_APPLICATION_CREDENTIALS_RAW}' > gcp_credentials.json
#RUN echo '${FIREBASE_APPLICATION_CREDENTIALS_RAW}' > firebase_credentials.json

RUN git clone --recursive https://github.com/AliaksandrSiarohin/first-order-model.git
RUN git clone --recursive https://github.com/AliaksandrSiarohin/motion-cosegmentation.git motion-co-seg

WORKDIR /run/first-order-model
RUN gdown https://drive.google.com/uc?id=1n2CqYEjM82X7sE40xrZpmnOxF6NekYW0
#RUN pip install -r requirements.txt

WORKDIR /run/motion-co-seg
RUN gdown https://drive.google.com/uc?id=1hwW8BjrlHzzG_Z4Z7uhHT73g1XLxO6PN
#RUN pip install -r requirements.txt

# within the motion-co-seg directory
RUN git clone --recursive https://github.com/AliaksandrSiarohin/face-makeup.PyTorch face_parsing
RUN gdown https://drive.google.com/uc?id=1n2CqYEjM82X7sE40xrZpmnOxF6NekYW0

SHELL ["/bin/bash", "-c"]

WORKDIR /run
COPY worker/environment_fsgan.yml .
RUN conda env create -f environment_fsgan.yml
#RUN conda install pytorch torchvision cudatoolkit=10.1 -c pytorch -y
#RUN conda install -c conda-forge yacs -y

RUN git clone https://github.com/YuvalNirkin/face_detection_dsfd
RUN git clone https://github.com/YuvalNirkin/fsgan.git
RUN mkdir fsgan_weights
WORKDIR /run/fsgan_weights
RUN gdown https://drive.google.com/uc?id=1eAw5qIGpBhV_mvZPH7ErC2MvVSjiaRpp
RUN gdown https://drive.google.com/uc?id=12voeEKaFkk1DxiygheJQXGO7K--9dWyF
RUN gdown https://drive.google.com/uc?id=1HLOGg0cOghAcRoBxt87PS0skw4YcdIa2
RUN gdown https://drive.google.com/uc?id=1sK0cxnszgmFohatmXnl8fiAS1zCQxM9_
RUN gdown https://drive.google.com/uc?id=1o3XkwpEEMuo8wvIXwEB19OlSvGYsQq2E
RUN gdown https://drive.google.com/uc?id=17M6RjE5bgSbeRKz0ZA04ofIPMg2Z3qnd
RUN gdown https://drive.google.com/uc?id=1HjMHskboFUzw5uPL-fqWb5NFOwQ4Z8bF
RUN gdown https://drive.google.com/uc?id=1cPfZ89LtzxvGTsOZw66ghcubBDawTAG5
