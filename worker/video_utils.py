import subprocess
import os, sys
import glob
import typing
from dataclasses import dataclass

from PIL import Image

@dataclass
class VideoUtils(object):
	source_image_path: str
	target_video_path: str 
	working_directory: str
	face_template: str
	video_template: str
	request_id: str
	s3: typing.Any

	def __post_init__(self):
		pass

	def split_video_to_images(self):
		"""
		video is stored on S3
		we rename locally to video.mp4
		"""
		try:
			os.chdir(self.working_directory + '/requests/' + self.request_id)
			#shutil.rmtree('raw_images')
			#os.mkdir('raw_images') # create directory if doesn't exist
			subprocess.call(["ffmpeg","-i","original_video.mp4","target_video_frames/filename%03d.png"])
			logger.debug('done with split_video_to_images')
			return True
		except Exception as e:
			logger.error(e, exc_info=True)

	def extract_audio_from_video(self):
		try:
			if not self.video_template: #giphy videos have no audio
				os.chdir(self.working_directory + '/requests/' + self.request_id)
				subprocess.call(["ffmpeg", "-i", "original_video.mp4", "-vn", "original_audio.mp3"])
				logger.debug('done with extract_audio_from_video')
			else:
				logger.debug('not extracting audio - using video template')
			return True
		except Exception as e:
			logger.error(e, exc_info=True)

	def combine_images_into_video(self):
		try:
			os.chdir(self.working_directory + '/requests/' + self.request_id + '/target_processed_video_frames')
			if not self.video_template:
				subprocess.call(["ffmpeg","-framerate","30000/1001","-f","image2","-i","filename%03d.png","-i","../original_audio.mp3","-acodec","aac","-vcodec","libx264","-pix_fmt","yuv420p","-strict","-2","-shortest","../processed_video.mp4","-y"])
				logger.debug('done with combine_images_into_video')
			else:
				# no audio to combine
				subprocess.call(["ffmpeg","-framerate","30000/1001","-f","image2","-i","filename%03d.png","-vcodec","libx264","-pix_fmt","yuv420p","-strict","-2","../processed_video.mp4","-y"])
				logger.debug('done with combine_images_into_video with no audio')
			return True
		except Exception as e:
			logger.error(e, exc_info=True)

	def crop_face_from_video(self):
		"""
		"""
		try:
			import face_recognition
			# don't try to make it perfect
			# instead just let it run inference, and store original coordinates of box
			# so that when paste back, looks fine.  for first order model this is all that's need.
			# later can try moving averages for smoothing, or look-ahead for smooth interpolation/panning

			# constraints:
			# maintain same image dimensions for each frame
			# maintain image dimensions divisible by 2 for each frame
			# ensure box with padding never goes beyond original image boundaries

			# for each frame, find dimensions of face... if bigger than existing, overwrite
			def round_two(x, base=2):
				return base * round(x/base)

			# create a box around first center with padding
			# if new box is within that first box, don't move camera
			box_width = box_height = 0
			#window_size = 48
			face_locations_list = {'top': [], 'left': [], 'bottom': [], 'right': []}
			first = True
			extra_height = 0
			extra_width = 0
			image_height = 0
			image_width = 0
			box_1 = None
			biggest_face = {'width': 0, 'height': 0}

			#box_1 is the currently-used bounding box with padding
			#box_2 is the current frame's bounding box

			def clamp(n, smallest, largest): 
				return max(smallest, min(n, largest))

			def make_box(face, padding, image_height, image_width):
				box = {'top': face['top']-padding['top'], 'left': face['left']-padding['left'], 'bottom': face['bottom']+padding['bottom'], 'right': face['right']+padding['right']}
				box['top'] = clamp(box['top'], 0, image_height)
				box['left'] = clamp(box['left'], 0, image_width)
				box['right'] = clamp(box['right'], 0, image_width)
				box['bottom'] = clamp(box['bottom'], 0, image_height)
				return box

			def first_pass():
				first = True
				nonlocal box_1
				nonlocal extra_height
				nonlocal extra_width
				nonlocal box_height
				nonlocal box_width
				nonlocal image_height
				nonlocal image_width
				# first pass - extract face locations
				for file_name in sorted(glob.glob(self.working_directory + '/requests/' + self.request_id + '/target_video_frames/*')): 
					#logger.debug(file_name)
					extension = file_name.split('/')[-1]
					# Load the jpg file into a numpy array
					image = face_recognition.load_image_file(file_name)
					image_height, image_width, channels = image.shape

					# Find all the faces in the image using a pre-trained convolutional neural network.
					# This method is more accurate than the default HOG model, but it's slower
					# unless you have an nvidia GPU and dlib compiled with CUDA extensions. But if you do,
					# this will use GPU acceleration and perform well.
					# See also: find_faces_in_picture.py
					face_locations = face_recognition.face_locations(image)#, number_of_times_to_upsample=0, model="cnn")
					#logger.debug("I found {} face(s) in this photograph.".format(len(face_locations)))
					top, right, bottom, left = face_locations[0]

					# find center of bounding box
					# then create new box of fixed height/width around that center point, so image always same size
					if first:
						logger.debug("A face is located at pixel location Top: {}, Left: {}, Bottom: {}, Right: {}".format(top, left, bottom, right))
						first=False
						box_height = (bottom-top)*1.1
						box_width = (right-left)*1.1
						box_midheight = int(box_height/2)
						#logger.debug('box midheight:' + str(box_midheight))
						box_midwidth = int(box_width/2)
						extra_height = (bottom-top)*0.05
						extra_width = (right-left)*0.05
						first_face = {'top': round_two(top), 'left': round_two(left), 'bottom': round_two(bottom), 'right': round_two(right)}
						box_1 = {'top': round_two(top-extra_height), 'left': round_two(left-extra_width), 'bottom': round_two(bottom+extra_height), 'right': round_two(right+extra_width)}
					
					# compare bounding box to desired height/width
					#logger.debug("A face is located at pixel location Top: {}, Left: {}, Bottom: {}, Right: {}".format(top, left, bottom, right))
					face_locations_list['top'].append(top)
					face_locations_list['left'].append(left)
					face_locations_list['bottom'].append(bottom)
					face_locations_list['right'].append(right)

					face_height = (bottom-top)
					face_width = (right-left)
					if face_height > biggest_face['height']:
						biggest_face['height'] = face_height
					if face_width > biggest_face['width']:
						biggest_face['width'] = face_width
				
				extra_height = round_two(biggest_face['height']*0.25)
				extra_width = round_two(biggest_face['width']*0.25)
				box_1 = make_box(first_face, {'top': extra_height, 'bottom': extra_height, 'left': extra_width, 'right': extra_width}, image_height, image_width)		
				return box_1
				
			# second pass, create a bounding box for each frame
			# check if it intersects with the current image position/dimensions (ie bounding box + padding)
			def second_pass(box_1):
				"""
				LATER: moving averages
				face_moving = {}
				for direction in ['top', 'left', 'bottom', 'right']:
					face_moving[direction] = pd.Series(face_locations_list[direction])
					windows = face_moving[direction].rolling(window_size)
					moving_averages = windows.mean()
					face_moving[direction] = moving_averages.tolist()"""
				nonlocal face_locations_list
				final_boxes = []
				for row in zip(face_locations_list['top'], face_locations_list['left'], face_locations_list['bottom'], face_locations_list['right']):
					#for row in zip(face_moving['top'], face_moving['left'], face_moving['bottom'], face_moving['right']):
					box_2 = {'top': row[0], 'left': row[1], 'bottom': row[2], 'right': row[3]}
					if box_2_inside_box_1(box_1, box_2):
						final_boxes.append(box_1) # keep the camera where it is
					else:
						# create a new box around the current position
						# but maintain the same image dimensions
						current_height = row[2] - row[0]
						current_width = row[3] - row[1]
						if current_height > biggest_face['height']:
							logger.debug('height > biggest face height')
						if current_width > biggest_face['width']:
							logger.debug('width > biggest face width')
						# figure out the correct padding, split between each side
						height_difference = (box_1['bottom']-box_1['top']) - current_height
						padding_top = height_difference//2
						padding_bottom = height_difference -  padding_top
						width_difference = (box_1['right']-box_1['left']) - current_width
						padding_left = width_difference//2
						padding_right = width_difference - padding_left
						if(padding_top < 0 or padding_bottom <0 or padding_left <0 or padding_right < 0):
							logger.debug('padding is negative')
						logger.debug('creating new box_1:')
						box_1 = make_box(box_2, {'top': padding_top, 'left': padding_left, 'right': padding_right, 'bottom': padding_bottom}, image_height, image_width)
						logger.debug(box_1)
						final_boxes.append(box_1)
				return final_boxes
				
			def final(final_boxes):
				for face, file_name in zip(final_boxes, sorted(glob.glob(self.working_directory + '/requests/' + self.request_id + '/target_video_frames/*'))): 
					logger.debug(face)
					extension = file_name.split('/')[-1]
					# Load the jpg file into a numpy array
					image = face_recognition.load_image_file(file_name)
					face_image = image[face['top']:face['bottom'], face['left']:face['right']]
					pil_image = Image.fromarray(face_image)
					pil_image.save(self.working_directory + '/requests/' + self.request_id + '/target_face_video_frames/' + extension, "PNG")
				
			# re-assemble cropped frames into a video, so that inference.py can resize to correct dimensions
			# TODO resize here to prevent having to read video frame by frame again
			def combine_images_into_video():
				os.chdir(self.working_directory + '/requests/' + self.request_id + '/target_face_video_frames')
				logger.debug(subprocess.check_output(["ffmpeg","-framerate","30000/1001","-f","image2","-i","filename%03d.png","-vcodec","libx264","-pix_fmt","yuv420p","-strict","-2",self.working_directory + '/requests/' + self.request_id + '/target_face_video_frames/original_video.mp4',"-y"]))

			def box_2_inside_box_1(box_1, box_2):
				return ((box_1['left'] <= box_2['left'] and box_1['right'] >= box_2['right']) and
					(box_1['top'] <= box_2['top'] and box_1['bottom'] >= box_2['bottom']))

			"""
			take original bounding box + spacing.  
			if new head doesn't lie within it, only then move the camera when it gets near border
			in direction of border, and subtract equivalent from opposite end
			that will automatically prevent the camera position changes every frame
			"""
			box_1 = first_pass()
			final_boxes = second_pass(box_1)
			final(final_boxes)
			combine_images_into_video()
			return final_boxes
		except Exception as e:
			logger.error(e, exc_info=True)

	def encode_audio(self, video_path, audio_path, output_path):
		import ffmpeg
		ffmpeg.concat(ffmpeg.input(video_path), ffmpeg.input(audio_path), v=1, a=1) \
			.output(output_path, strict='-2').run(overwrite_output=True)