import subprocess
import os, sys
import os.path
import shutil
import numpy as np
from PIL import Image
import shutil
import boto3
import logging
import uuid
import glob
import argparse
import typing
import requests
from datetime import datetime
from dataclasses import dataclass

logging.getLogger('s3transfer').setLevel(logging.CRITICAL)

from google_logger import setup_logging
import inference, db_models, file_utils, video_utils
from db_models import *
from file_utils import FileUtils
from video_utils import VideoUtils
logger = setup_logging()
inference.logger = logger
db_models.logger = logger
file_utils.logger = logger
video_utils.logger = logger

WORKING_DIRECTORY = os.getenv('AZ_BATCH_TASK_WORKING_DIR')
INPUT_S3_BUCKET = os.getenv('INPUT_S3_BUCKET')
OUTPUT_S3_BUCKET = os.getenv('OUTPUT_S3_BUCKET')
AWS_ACCESS_KEY_ID = os.getenv('AWS_ACCESS_KEY_ID')
AWS_SECRET_ACCESS_KEY = os.getenv('AWS_SECRET_ACCESS_KEY')
CDN_URL_PREFIX = os.getenv('CDN_URL_PREFIX')

# Get the service client
s3 = boto3.client('s3', aws_access_key_id=AWS_ACCESS_KEY_ID, aws_secret_access_key=AWS_SECRET_ACCESS_KEY, region_name='us-west-2')

@dataclass
class Swap(object):
	request_id: str
	swap_record: typing.Any
	source_video: typing.Any = None
	target_video: typing.Any = None

	def __post_init__(self):
		self.model = self.swap_record.model
		self.video_template = None
		self.face_template = None
		self.user_id = None
		self.source_image_path = WORKING_DIRECTORY + '/requests/' + self.request_id + '/face_images/face.png'
		self.source_video_path = WORKING_DIRECTORY + '/requests/' + self.request_id + '/original_source_video.mp4'
		self.target_video_path = WORKING_DIRECTORY + '/requests/' + self.request_id + '/original_target_video.mp4'
		self.WORKING_DIRECTORY = WORKING_DIRECTORY # for video & file_utils
		if self.source_video:
			self.source_video_path = self.source_video.file_location_url
			self.source_video_start = self.source_video.start_time
			self.source_video_end = self.source_video.end_time
		else:
			self.source_video_start = None
			self.source_video_end = None
		if self.target_video:
			self.target_video_path = self.target_video.file_location_url
			self.target_video_start = self.target_video.start_time
			self.target_video_end = self.target_video.end_time
		else:
			self.target_video_start = None
			self.target_video_end = None
		self.s3 = s3
		self.file_utils = FileUtils(self.source_image_path, self.source_video_path, self.target_video_path, self.WORKING_DIRECTORY, self.face_template, self.video_template, self.request_id, self.s3, self.source_video_start, self.source_video_end, self.target_video_start, self.target_video_end)
		self.video_utils = VideoUtils(self.source_image_path, self.target_video_path, self.WORKING_DIRECTORY, self.face_template, self.video_template, self.request_id, self.s3)

	def run_pipeline(self):
		"""
		keeping all methods in the same class
		allows us to chain these easily into a pipeline
		without having to pass variables back & forth
		"""
		try:
			logger.debug('in new run')

			if self.model=='first_order':
				logger.debug('first order model')
				return self.file_utils.download_templates(self.swap_record) and \
				self.file_utils.download_video() and \
				self.file_utils.download_face_images() and \
				self.video_utils.split_video_to_images() and \
				self.video_utils.extract_audio_from_video() and \
				self.run_model_inference() and \
				self.video_utils.combine_images_into_video()
			elif self.model=='fsgan':
				logger.debug('fsgan model')
				return self.file_utils.youtube_download_cut() and \
				self.run_model_inference()
		except Exception as e:
			logger.error(e, exc_info=True)

	def run_model_inference(self):
		try:
			self.source_image_path = self.file_utils.source_image_path
			self.video_utils.video_template = self.file_utils.video_template
			self.video_utils.face_template = self.file_utils.face_template
			self.source_video_path = WORKING_DIRECTORY + '/requests/' + self.request_id + '/original_source_video.mp4'
			self.target_video_path = WORKING_DIRECTORY + '/requests/' + self.request_id + '/original_target_video.mp4'
			model = inference.Inference.create(self.model, self.request_id, self.source_image_path, self.source_video_path, self.target_video_path, WORKING_DIRECTORY, self.video_utils)
			return model.execute()
		except Exception as e:
			logger.error(e, exc_info=True)

def process(request_id):
	"""
	/run/requests/{request_id}/original_video.mp4
	/run/requests/{request_id}/original_audio.mp3
	/run/requests/{request_id}/face_images/face.png
	/run/requests/{request_id}/target_video_frames/*.png
	/run/requests/{request_id}/target_face_video_frames/*.png
	/run/requests/{request_id}/target_processed_video_frames/*.png
	/run/requests/{request_id}/processed_video.mp4
	"""
	try:
		logger.debug('in process')
		os.makedirs(WORKING_DIRECTORY + '/requests/' + request_id, exist_ok=True)
		directories = ['face_images', 'target_video_frames', 'target_face_video_frames', 'target_processed_video_frames']
		for dir in directories:
			os.makedirs(WORKING_DIRECTORY + '/requests/' + request_id + '/' + dir, exist_ok=True)

		# video and images are uploaded to an S3 folder named request_id

		# run each step until one returns False, at which point stop (as each is dependent on the output of the previous)
		session = connect_to_database()
		record = session.query(Swaps).filter(Swaps.request_id==request_id).first()
		if record:
			# TODO replace with join
			source_video_id = record.source_video_id
			target_video_id = record.target_video_id
			source_video = None
			target_video = None
			if source_video_id:
				logger.debug('got source video id')
				source_video = session.query(Videos).filter(Videos.id==source_video_id).first()
			if target_video_id:
				logger.debug('got target video id')
				target_video = session.query(Videos).filter(Videos.id==target_video_id).first()
			
			swap = Swap(request_id, record, source_video, target_video)
			result = swap.run_pipeline()
			url = None
			if result:
				url = swap.file_utils.upload_video()
				# store in database
				new_video = get_or_create(session, Videos, user_id=record.user_id, file_location_url=url, upload_date=datetime.utcnow(), uuid=request_id)
				record.status = 'processed'
				record.result_video_id = new_video.id
				session.commit()
				session.close()
				logger.debug('done with swap_face')
			else:
				logger.debug('no result from swap - not uploading result')
		else:
			logger.error('didnt find matching request id')
	except Exception as e:
		logger.error(e, exc_info=True)

if __name__ == '__main__':
	logger.debug('starting worker')
	parser = argparse.ArgumentParser(description='')
	# Required positional argument
	parser.add_argument('request_id', action='store', help='S3 folder key')
	commands = parser.parse_args()
	request_id = commands.request_id
	process(request_id)
