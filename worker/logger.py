import logging as logging
import google.cloud.logging
from google.cloud.logging.handlers import CloudLoggingHandler
from datetime import datetime
# https://github.com/googleapis/google-cloud-python/issues/6354#issuecomment-502073999
from google.cloud.logging.handlers.transports.background_thread import BackgroundThreadTransport, _Worker

def custom_enqueue(self, record, message, resource=None, labels=None, trace=None, span_id=None):
    entry = {"info": {"message": str(message), "python_logger": "worker"},"severity": record.levelname,"resource": resource,"labels": labels,"trace": trace,"span_id": span_id,"timestamp": datetime.utcfromtimestamp(record.created)}
    entry["info"]["filePath"] = record.filename
    entry["info"]["lineNumber"] = record.lineno
    entry["info"]["functionName"] = record.funcName
    if record.args:
        entry["info"]["args"] = record.args
    self._queue.put_nowait(entry)

def setup_logging():
    # monkeypatch enqueue function
    #_Worker.enqueue = custom_enqueue

    client = google.cloud.logging.Client()
    handler = CloudLoggingHandler(client,transport=BackgroundThreadTransport)
    google.cloud.logging.handlers.setup_logging(handler, excluded_loggers=('google.cloud', 'google.auth', 'google_auth_httplib2','urllib3.connectionpool'))
    logging.getLogger().setLevel(logging.DEBUG)
    return logging