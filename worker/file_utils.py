import os
import glob
import typing
from dataclasses import dataclass
import subprocess

import boto3
import requests

WORKING_DIRECTORY = os.getenv('AZ_BATCH_TASK_WORKING_DIR')
INPUT_S3_BUCKET = os.getenv('INPUT_S3_BUCKET')
OUTPUT_S3_BUCKET = os.getenv('OUTPUT_S3_BUCKET')
AWS_ACCESS_KEY_ID = os.getenv('AWS_ACCESS_KEY_ID')
AWS_SECRET_ACCESS_KEY = os.getenv('AWS_SECRET_ACCESS_KEY')
CDN_URL_PREFIX = os.getenv('CDN_URL_PREFIX')

@dataclass
class FileUtils(object):
	source_image_path: str
	source_video_path: str
	target_video_path: str
	working_directory: str
	face_template: str
	video_template: str
	request_id: str
	s3: typing.Any
	source_video_start: str = None
	source_video_end: str = None
	target_video_start: str = None
	target_video_end: str = None

	def __post_init__(self):
		pass

	def download_s3_folder(self, prefix, local, bucket):
		"""
		params:
		- prefix: pattern to match in s3
		- local: local path to folder in which to place files
		- bucket: s3 bucket with target contents
		"""
		try:
			s3_resource = boto3.resource('s3', aws_access_key_id=AWS_ACCESS_KEY_ID, aws_secret_access_key=AWS_SECRET_ACCESS_KEY, region_name='us-west-2')
			bucket = s3_resource.Bucket(bucket)
			for file in bucket.objects.filter(Prefix = prefix):
				logger.debug('download_s3_folder key:' + str(file.key))
				if prefix != file.key and prefix != file.key.strip('/'): # not the folder itself, or folder with backslash
					path, filename = os.path.split(file.key)
					logger.debug('downloading to:' + local + '/' + filename)
					bucket.download_file(file.key, local + '/' + filename)
		except Exception as e:
			logger.error(e, exc_info=True)

	def download_face_images(self):
		try:
			if not self.face_template:
				path = self.working_directory + '/requests/' + self.request_id + '/face_images'
				self.download_s3_folder(self.request_id + '/face_images', path, INPUT_S3_BUCKET)
				logger.debug('done with download_images')
				# set the source_image_path to the first image
				for f in glob.glob(path + '/*'):
					self.source_image_path = f
					break
			else:
				logger.debug('using face template - not downloading face images')
			return True
		except Exception as e:
			logger.error(e, exc_info=True)

	def download_video(self):
		try:
			if not self.video_template:
				os.chdir(self.working_directory + '/requests/' + self.request_id)
				self.s3.download_file(INPUT_S3_BUCKET, self.request_id + '/original_video.mp4', 'original_video.mp4')
				logger.debug('done with download_video')
			else:
				logger.debug('using video template - not downloading video')
			return True
		except Exception as e:
			logger.error(e, exc_info=True)

	def upload_video(self):
		"""
		"""
		try:
			os.chdir(self.working_directory + '/requests/' + self.request_id)
			path = self.request_id + '/' + 'processed_video.mp4'
			self.s3.upload_file('processed_video.mp4', OUTPUT_S3_BUCKET, path, ExtraArgs={'ACL':'public-read'})
			url = CDN_URL_PREFIX + path
			logger.debug('done with upload_video')
			return url
		except Exception as e:
			logger.error(e, exc_info=True)

	def youtube_download_cut(self):
		"""
		"""
		try:
			os.chdir(self.working_directory + '/requests/' + self.request_id)
			for video_type in ['source_video_path','target_video_path']:
				if getattr(self, video_type):
					video_name = video_type.split('_')[0]
					source_url = getattr(self, video_type)
					source_start = getattr(self, video_name + '_video_start')
					source_end = getattr(self, video_name + '_video_end')

					if not source_start and source_end:
						logger.debug('saving youtube video without any cutting')
						subprocess.call(["youtube-dl", source_url, "--merge-output-format","mp4","-o","original_" + video_name + "_video.mp4"])
					else:
						logger.debug('saving youtube video with cutting')
						subprocess.call(["youtube-dl", source_url, "--merge-output-format","mp4","-o",video_name + "_video_tmp.mp4"])
						subprocess.call(["ffmpeg","-y","-i",video_name + "_video_tmp.mp4","-ss",source_start,"-to",source_end,"-r","25","original_" + video_name + "_video.mp4"])
						subprocess.call(["rm",video_name + "_video_tmp.mp4"])
			return True
		except Exception as e:
			logger.error(e, exc_info=True)

	def giphy_download(self):
		"""
		"""
		try:
			logger.debug('downloading MP4 from giphy')
			os.chdir(self.working_directory + '/requests/' + self.request_id)
			r = requests.get(self.video_template)
			open('original_video.mp4','wb').write(r.content)
		except Exception as e:
			logger.error(e, exc_info=True)

	def download_templates(self, record):
		"""
		"""
		try:
			logger.debug('in download_templates')
			self.video_template = record.video_template
			self.face_template = record.face_template
			self.user_id = record.user_id
			if self.video_template:
				# download from Giphy/Youtube
				self.giphy_download()
			if self.face_template:
				# download from S3
				logger.debug('downloading face templates from S3')
				path = self.working_directory + '/requests/' + self.request_id + '/face_images'
				self.download_s3_folder('face_templates/' + self.face_template, path, INPUT_S3_BUCKET)
				for f in glob.glob(path + '/*'):
					self.source_image_path = f
					break
			return True
		except Exception as e:
			logger.error(e, exc_info=True)