import subprocess
import os, sys
import os.path
from datetime import datetime
import shutil
import logging
import uuid
import glob
import typing
import argparse
import warnings
from dataclasses import dataclass
from base64 import b64encode

import numpy as np
import torch
import torch.nn.functional as F
import imageio
import matplotlib.pyplot as plt
import ffmpeg
import matplotlib.animation as animation
from skimage.transform import resize
from skimage import img_as_ubyte
import matplotlib.patches as mpatches
from PIL import Image, ImageDraw, ImageFilter

warnings.filterwarnings("ignore")

WORKING_DIRECTORY = os.getenv('AZ_BATCH_TASK_WORKING_DIR')
INPUT_S3_BUCKET = os.getenv('INPUT_S3_BUCKET')
OUTPUT_S3_BUCKET = os.getenv('OUTPUT_S3_BUCKET')
AWS_ACCESS_KEY_ID = os.getenv('AWS_ACCESS_KEY_ID')
AWS_SECRET_ACCESS_KEY = os.getenv('AWS_SECRET_ACCESS_KEY')
CDN_URL_PREFIX = os.getenv('CDN_URL_PREFIX')
TESTING = os.getenv('TESTING')

subclasses = {}

def check_nvidia_gpu():
	"""
	"""
	try:
		logger.debug(subprocess.call(["nvidia-smi"]))
		import torch
		torch.cuda.is_available()
	except Exception as e:
		logger.error(e, exc_info=True)

@dataclass
class Inference(object):
	request_id: str
	source_image_path: str
	source_video_path: str
	target_video_path: str
	WORKING_DIRECTORY: str
	video_utils: typing.Any

	def __init_subclass__(cls, **kwargs):
		super().__init_subclass__(**kwargs)
		subclasses[cls._model] = cls

	@classmethod
	def create(cls, model, request_id, source_image_path, source_video_path, target_video_path, WORKING_DIRECTORY, video_utils):
		#if model not in cls.subclasses:
		if model not in subclasses:
			raise ValueError('Bad protocol {}'.format(model))
		return subclasses[model](request_id, source_image_path, source_video_path, target_video_path, WORKING_DIRECTORY, video_utils)

@dataclass
class FirstOrderModel(Inference):
	"""
	https://github.com/AliaksandrSiarohin/first-order-model
	translated from this Jupyter notebook: https://colab.research.google.com/drive/17q0KqJsxAM64mlDo4CWaP7vDJODpxlJS#scrollTo=Tc2INryCmQOu&uniqifier=3
	"""
	_model = 'first_order'

	def __post_init__(self):
		sys.path.append("/run/motion-co-seg")
		self.swap_ears = False
		self.feather_blur = True
		self.target_video_path = self.WORKING_DIRECTORY + '/requests/' + self.request_id + '/target_face_video_frames/original_video.mp4'

	def process_inputs(self):
		"""
		"""
		try:
			#Resize image and video to 256x256
			self.source_image = resize(imageio.imread(self.source_image_path), (256, 256))[..., :3]
			self.boxes = self.video_utils.crop_face_from_video()
			self.target_video = [resize(frame, (256, 256))[..., :3] for frame in imageio.mimread(self.target_video_path)]
			logger.debug('done with process_inputs')
			return True
		except Exception as e:
			logger.error(e, exc_info=True)

	def initialize_model(self):
		"""
		"""
		try:
			if TESTING!=1:
				from part_swap import load_checkpoints, make_video, load_face_parser
				os.chdir('/run/motion-co-seg')
				self.reconstruction_module, self.segmentation_module = load_checkpoints(config='/run/motion-co-seg/config/vox-256-sem-10segments.yaml', 
															checkpoint='/run/first-order-model/vox-first-order.pth.tar',
															blend_scale=0.125, first_order_motion_model=True)

				self.face_parser = load_face_parser(cpu=False)
			logger.debug('done with initialize_model')
			return True
		except Exception as e:
			logger.error(e, exc_info=True)

	def debug_model(self):
		"""
		"""
		try:
			def visualize_segmentation(image, network, supervised=False, hard=True, colormap='gist_rainbow'):
				with torch.no_grad():
					inp = torch.tensor(image[np.newaxis].astype(np.float32)).permute(0, 3, 1, 2).cuda()
					if supervised:
						inp = F.interpolate(inp, size=(512, 512))
						inp = (inp - network.mean) / network.std
						mask = torch.softmax(network(inp)[0], dim=1)
						mask = F.interpolate(mask, size=image.shape[:2])
					else:
						mask = network(inp)['segmentation']
						mask = F.interpolate(mask, size=image.shape[:2], mode='bilinear')
				
				if hard:
					mask = (torch.max(mask, dim=1, keepdim=True)[0] == mask).float()
				
				colormap = plt.get_cmap(colormap)
				num_segments = mask.shape[1]
				mask = mask.squeeze(0).permute(1, 2, 0).cpu().numpy()
				color_mask = 0
				patches = []
				for i in range(num_segments):
					if i != 0:
						color = np.array(colormap((i - 1) / (num_segments - 1)))[:3]
					else:
						color = np.array((0, 0, 0))
					patches.append(mpatches.Patch(color=color, label=str(i)))
					color_mask += mask[..., i:(i+1)] * color.reshape(1, 1, 3)
				
				fig, ax = plt.subplots(1, 2, figsize=(12,6))

				ax[0].imshow(color_mask)
				ax[1].imshow(0.3 * image + 0.7 * color_mask)
				ax[1].legend(handles=patches)
				ax[0].axis('off')
				ax[1].axis('off')

			visualize_segmentation(self.source_image, self.segmentation_module, hard=True)
			plt.show()

			# visualize_segmentation(source_image, face_parser, supervised=True, hard=True, colormap='tab20')
			# plt.show()
		except Exception as e:
			logger.error(e, exc_info=True)

	def run_inference(self):
		"""
		"""
		try:
			logger.debug('in run_inference')
			if TESTING!=1:
				swap_index = list(range(1, 16)) # 1 to 15
				if self.swap_ears==False:
					swap_index.remove(7)
					swap_index.remove(8)
				self.predictions = make_video(swap_index=swap_index, source_image = self.source_image,
										target_video = self.target_video, use_source_segmentation=True, segmentation_module=self.segmentation_module,
										reconstruction_module=self.reconstruction_module, face_parser=self.face_parser)
			logger.debug('done with run_inference')
			return True
		except Exception as e:
			logger.error(e, exc_info=True)

	def paste_face_into_video(self):
		"""
		"""
		try:
			if TESTING!=1:
				box_height = self.boxes[0]['bottom'] - self.boxes[0]['top']
				box_width = self.boxes[0]['right'] - self.boxes[0]['left']

				logger.debug('predictions length:' + str(len(self.predictions)))
				logger.debug('boxes length:' + str(len(self.boxes)))
				logger.debug('target video frames length:' + str(len(glob.glob(self.WORKING_DIRECTORY + '/requests/' + self.request_id + '/target_video_frames/*'))))

				for face_swapped, file_name, box in zip(self.predictions, sorted(glob.glob(self.WORKING_DIRECTORY + '/requests/' + self.request_id + '/target_video_frames/*')), self.boxes):
					swapped_image = Image.fromarray(img_as_ubyte(face_swapped))
					original_image = Image.open(file_name)
					# resize to original dimensions of bounding box
					swapped_resized_image = swapped_image.resize((box_width, box_height))

					if self.feather_blur:
						# create the mask  
						w, h = original_image.size
						padding = 5
						shape = [(box['left'] + padding, box['top']+padding), (box['right']-padding, box['bottom']-padding)]
						# creating new Image object 
						mask = Image.new("L", (w, h), color=255) 
						# create rectangle image 
						canvas = ImageDraw.Draw(mask)
						canvas.rectangle(shape, fill ="#000000")
						mask_blur = mask.filter(ImageFilter.GaussianBlur(padding))

						blank = original_image.copy()
						blank.paste(swapped_resized_image, (box['left'], box['top']))

						combined_image = Image.composite(original_image, blank, mask_blur)
						combined_image.save(self.WORKING_DIRECTORY + '/requests/' + self.request_id + '/target_processed_video_frames/' + file_name.split('/')[-1])
					else:
						#paste back into original video at location specified by boxes
						original_image.paste(swapped_resized_image, (box['left'], box['top']))
						#save image
						original_image.save(self.WORKING_DIRECTORY + '/requests/' + self.request_id + '/target_processed_video_frames/' + file_name.split('/')[-1])
			return True
		except Exception as e:
			logger.error(e, exc_info=True)

	def save_video(self):
		try:
			if TESTING!=1:
				# Saving video of just the face
				imageio.mimsave(self.WORKING_DIRECTORY + '/requests/' + self.request_id + '/processed_face_video.mp4', [img_as_ubyte(frame) for frame in self.predictions], fps=30)
				# processed_video.mp4 will be saved by video_utils.combine_images_into_video()
			return True
		except Exception as e:
			logger.error(e, exc_info=True)

	def execute(self):
		"""
		"""
		try:
			result = self.process_inputs() and \
			self.initialize_model() and \
			self.run_inference() and \
			self.paste_face_into_video() and \
			self.save_video()
			logger.debug('done with model inference')
			return result
		except Exception as e:
			logger.error(e, exc_info=True)

class FSGAN(Inference):
	_model = 'fsgan'

	def __post_init__(self):
		sys.path.append("/run/fsgan")

	def initialize_model(self):
		try:
			if TESTING!=1:
				logger.debug('fsgan initialize model')
				from fsgan.inference.swap import FaceSwapping
				from fsgan.criterions.vgg_loss import VGGLoss

				#Path to the weights directory (make sure it is correct):
				weights_dir = '/run/fsgan_weights'
				#Number of finetune iterations on the source subject:
				finetune_iterations = 800
				#If True, the inner part of the mouth will be removed from the segmentation:
				seg_remove_mouth = True

				detection_model = os.path.join(weights_dir, 'WIDERFace_DSFD_RES152.pth')
				pose_model = os.path.join(weights_dir, 'hopenet_robust_alpha1.pth')
				lms_model = os.path.join(weights_dir, 'hr18_wflw_landmarks.pth')
				seg_model = os.path.join(weights_dir, 'celeba_unet_256_1_2_segmentation_v2.pth')
				reenactment_model = os.path.join(weights_dir, 'nfv_msrunet_256_1_2_reenactment_v2.1.pth')
				completion_model = os.path.join(weights_dir, 'ijbc_msrunet_256_1_2_inpainting_v2.pth')
				blending_model = os.path.join(weights_dir, 'ijbc_msrunet_256_1_2_blending_v2.pth')
				criterion_id_path = os.path.join(weights_dir, 'vggface2_vgg19_256_1_2_id.pth')
				criterion_id = VGGLoss(criterion_id_path)

				self.face_swapping = FaceSwapping(
					detection_model=detection_model, pose_model=pose_model, lms_model=lms_model,
					seg_model=seg_model, reenactment_model=reenactment_model,
					completion_model=completion_model, blending_model=blending_model,
					criterion_id=criterion_id,
					finetune=True, finetune_save=True, finetune_iterations=finetune_iterations,
					seg_remove_mouth=seg_remove_mouth, batch_size=16, seg_batch_size=48,
					encoder_codec='mp4v')
			return True
		except Exception as e:
			logger.error(e, exc_info=True)

	def process_inputs(self):
		try:
			return True
		except Exception as e:
			logger.error(e, exc_info=True)

	def run_inference(self):
		try:
			if TESTING!=1:
				logger.debug('fsgan run inference')
				# Do face swapping
				#Toggle whether to finetune the reenactment generator:
				finetune = True
				#Source selection method ["longest" | sequence number]:
				select_source = 'longest'
				#Target selection method ["longest" | sequence number]:
				select_target = 'longest'
				#source_path = '/run/fsgan/docs/examples/shinzo_abe.mp4'
				#target_path = '/run/fsgan/docs/examples/conan_obrien.mp4'
				output_tmp_path = self.WORKING_DIRECTORY + '/requests/' + self.request_id + '/output_tmp.mp4'
				output_path = self.WORKING_DIRECTORY + '/requests/' + self.request_id + '/processed_video.mp4'
				self.face_swapping(self.source_video_path, self.target_video_path, output_tmp_path,
							select_source, select_target, finetune)

				# Encode with audio and display result
				self.video_utils.encode_audio(output_tmp_path, self.target_video_path, output_path)
				os.remove(output_tmp_path)
			return True
		except Exception as e:
			logger.error(e, exc_info=True)

	def execute(self):
		"""
		"""
		try:
			result = self.process_inputs() and \
			self.initialize_model() and \
			self.run_inference()
			logger.debug('done with fsgan model inference')
			return result
		except Exception as e:
			logger.error(e, exc_info=True)